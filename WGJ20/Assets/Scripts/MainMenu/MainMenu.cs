﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private Button btnExitGame;
    [SerializeField] private Button btnCredits;
    [SerializeField] private Button btnPlay;
    [SerializeField] private Button btnTutorial;

    [SerializeField] private GameObject tutorialView;
    [SerializeField] private Button btnCloseTutorial;

    [SerializeField] private GameObject creditsView;
    [SerializeField] private Button btnCloseCredits;
    [SerializeField] private Button btnMoreCredits;

    [SerializeField] private GameObject cosoNegro;
    [SerializeField] private GameObject moreCreditsView;
    [SerializeField] private Button btnCloseMoreCredits;
    [SerializeField] private Button btnBackToCredits;
    
    [SerializeField] private Transform customCursor;
    
    private void Awake()
    {
        btnExitGame.onClick.AddListener(OnBtnExitClick);
        btnCloseCredits.onClick.AddListener(OnBtnCloseCreditsClick);
        btnCredits.onClick.AddListener(OnBtnCreditsClick);
        btnMoreCredits.onClick.AddListener(OnBtnMoreCreditsClick);
        btnCloseMoreCredits.onClick.AddListener(OnCloseMoreCreditsClick);
        btnBackToCredits.onClick.AddListener(OnBtnBackToCreditsClick);
        btnPlay.onClick.AddListener(OnBtnPlayClick);
        btnTutorial.onClick.AddListener(OnBtnTutorialClick);
        btnCloseTutorial.onClick.AddListener(OnCloseTutorialClick);
        
        creditsView.SetActive(false);
        cosoNegro.SetActive(false);
        moreCreditsView.SetActive(false);
        tutorialView.SetActive(false);

        Cursor.visible = false;
    }

    private void Update()
    {
        customCursor.position = Input.mousePosition;
    }

    private void OnBtnPlayClick()
    {
        SceneManager.LoadScene("Game");
    }

    private void OnBtnTutorialClick()
    {
        tutorialView.SetActive(true);
    }
    
    private void OnCloseTutorialClick()
    {
        tutorialView.SetActive(false);
    }
    
    private void OnBtnCreditsClick()
    {
        creditsView.SetActive(true);
    }
    
    private void OnBtnCloseCreditsClick()
    {
        creditsView.SetActive(false);
    }
    
    private void OnCloseMoreCreditsClick()
    {
        cosoNegro.SetActive(false);
        moreCreditsView.SetActive(false);
        OnBtnCloseCreditsClick();
    }
    
    private void OnBtnMoreCreditsClick()
    {
        cosoNegro.SetActive(true);
        moreCreditsView.SetActive(true);
    }
    
    private void OnBtnBackToCreditsClick()
    {
        cosoNegro.SetActive(false);
        moreCreditsView.SetActive(false);
    }

    private void OnBtnExitClick()
    {
        Application.Quit();
    }
}
