﻿using UnityEngine;
using UnityEngine.UI;

public class SpecialToggle : MonoBehaviour
{
    [SerializeField] private Image background;

    private Toggle toggle;
    
    public void OnToggleValueChange()
    {
        if (toggle == null)
            toggle = GetComponent<Toggle>();
        
        if(toggle.isOn)
            background.color = new Color(1f, 1f, 1f, 0);
        else
            background.color = new Color(1f, 1f, 1f, 1);
    }
}
