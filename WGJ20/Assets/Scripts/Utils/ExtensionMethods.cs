﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ExtensionMethods
{
    public static T PickRandom<T>(this IEnumerable<T> source)
    {
        return source.PickRandom(1).Single();
    }

    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
    {
        return source.Shuffle().Take(count);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => Guid.NewGuid());
    }

    public static string ToLocalizedString(this DayOfWeek source)
    {
        switch (source)
        {
            case DayOfWeek.Friday:
            {
                return "Viernes";
            }
            case DayOfWeek.Saturday:
            {
                return "Sábado";
            }
            case DayOfWeek.Sunday:
            {
                return "Domingo";
            }
            case DayOfWeek.Monday:
            {
                return "Lunes";
            }
            case DayOfWeek.Tuesday:
            {
                return "Martes";
            }
            case DayOfWeek.Wednesday:
            {
                return "Miércoles";
            }
            case DayOfWeek.Thursday:
            {
                return "Jueves";
            }
            default:
                return source.ToString();
        }
    }
}
