using UnityEngine;

namespace DefaultNamespace
{
    public class InMemoryData
    {
        private static InMemoryData _instance;

        public static InMemoryData Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new InMemoryData();

                return _instance;
            }
        }

        public Photo ResultingPhoto { get; set; }
        public Photo InitialPhoto { get; set; }

        public int totalPoints;
    }
}