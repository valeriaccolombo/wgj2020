﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultSceneView : MonoBehaviour
{
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [SerializeField] private GameObject superhappy;
    [SerializeField] private GameObject happy;
    [SerializeField] private GameObject sad;
    
    private void Start()
    {
        Cursor.visible = true;
        
        InMemoryData.Instance.InitialPhoto.transform.SetParent(start, false);
        InMemoryData.Instance.ResultingPhoto.transform.SetParent(end, false);
        
        InMemoryData.Instance.InitialPhoto.transform.localPosition = Vector3.zero;
        InMemoryData.Instance.ResultingPhoto.transform.localPosition = Vector3.zero;
        
        Debug.Log("Resultado final: " + InMemoryData.Instance.totalPoints.ToString());
        superhappy.SetActive(InMemoryData.Instance.totalPoints >= 9);
        happy.SetActive(InMemoryData.Instance.totalPoints < 9 && InMemoryData.Instance.totalPoints >= 4);
        sad.SetActive(InMemoryData.Instance.totalPoints < 4);
    }

    public void BtnBackToMenuClick()
    {
        Destroy(InMemoryData.Instance.InitialPhoto.gameObject);
        Destroy(InMemoryData.Instance.ResultingPhoto.gameObject);
        
        SceneManager.LoadScene("Menu");
    }
}
