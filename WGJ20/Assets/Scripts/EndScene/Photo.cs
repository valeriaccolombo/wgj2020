﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Photo : MonoBehaviour
{
    [SerializeField] private List<Image> objects;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);        
    }

    public void CopyFrom(Transform assetOrig)
    {
        foreach (var piece in objects)
        {
            var orig = assetOrig.Find(piece.name);
            piece.color = orig.gameObject.GetComponent<Image>().color;
            piece.gameObject.SetActive(orig.gameObject.activeSelf);
        }
    }
}
