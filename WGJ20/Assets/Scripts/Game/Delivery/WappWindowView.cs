using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Delivery
{
    public class WappWindowView : MonoBehaviour
    {
        //friendsList
        [SerializeField] private ChatPreviewButton chatButton01;
        [SerializeField] private ChatPreviewButton chatButton02;
        [SerializeField] private ChatPreviewButton chatButton03;
        [SerializeField] private ChatPreviewButton chatButton04;
        [SerializeField] private GameObject badgeTotalUnread;
        [SerializeField] private TMP_Text badgeTotalUnreadText;
        
        //chatView
        [SerializeField] private WappChatMessageView firendMessageChatPrefab;
        [SerializeField] private WappChatMessageView userMessageChatPrefab;
        [SerializeField] private GameObject daySeparatorPrefab;
        [SerializeField] private GameObject chatWindow;
        [SerializeField] private Button btnCloseChatWindow;
        [SerializeField] private Transform chatContainer;
        [SerializeField] private TMP_Text chatName;
        [SerializeField] private TMP_Dropdown messagesDropdown;
        [SerializeField] private ScrollRect chatScroll;
        private Action<WappFriend> onCloseFriendChat;
        private Action<WappFriend, int> onSendMessage;
        
        private Dictionary<int, ChatPreviewButton> friendsList;
        
        public void LoadFriendsList(List<WappFriend> friends, Action<WappFriend> onClickFriendChat, Action<WappFriend> onCloseFriendChat, Action<WappFriend, int> onSendMessage)
        {
            this.onCloseFriendChat = onCloseFriendChat;
            this.onSendMessage = onSendMessage;
            
            friendsList = new Dictionary<int, ChatPreviewButton>();
            
            chatButton01.Initialize(friends[0]);
            chatButton01.ButtonComponent.onClick.AddListener(() => onClickFriendChat(friends[0]));
            friendsList.Add(friends[0].friendID, chatButton01);
            
            chatButton02.Initialize(friends[1]);
            chatButton02.ButtonComponent.onClick.AddListener(() => onClickFriendChat(friends[1]));
            friendsList.Add(friends[1].friendID, chatButton02);

            chatButton03.Initialize(friends[2]);
            chatButton03.ButtonComponent.onClick.AddListener(() => onClickFriendChat(friends[2]));
            friendsList.Add(friends[2].friendID, chatButton03);
            
            chatButton04.Initialize(friends[3]);
            chatButton04.ButtonComponent.onClick.AddListener(() => onClickFriendChat(friends[3]));
            friendsList.Add(friends[3].friendID, chatButton04);  
            
            badgeTotalUnread.SetActive(false);
        }

        public void RefreshNotis(List<WappFriend> friends)
        {
            foreach (var friend in friends)
            {
                friendsList[friend.friendID].RefreshData(friend);
            }

            var friendsWithNotis = friends.FindAll(f => f.UnreadNotis > 0).Count();
            badgeTotalUnread.SetActive(friendsWithNotis > 0);
            badgeTotalUnreadText.text = friendsWithNotis.ToString();
        }

        private WappFriend friendFromCurrentChat;

        public void ShowChatForFriend(WappFriend friend, List<int> responseOptions, List<int> myMessageOptions,
            Dictionary<int, string> tids)
        {
            friendFromCurrentChat = friend;

            btnCloseChatWindow.onClick.AddListener(CloseFriendChat);

            chatWindow.SetActive(true);
            chatName.text = friend.name;

            ReloadMessages(friend, responseOptions, myMessageOptions, tids);
        }

        public void CloseFriendChat()
        {
            if (friendFromCurrentChat != null)
            {
                btnCloseChatWindow.onClick.RemoveAllListeners();
                onCloseFriendChat(friendFromCurrentChat);            
                friendFromCurrentChat = null;                
            }
        }
        
        public void ReloadMessages(WappFriend friend, List<int> responseOptions, List<int> myMessageOptions,
            Dictionary<int, string> tids)
        {
            foreach (Transform child in chatContainer) {
                Destroy(child.gameObject);
            }

            StartCoroutine(LoadMessages(friend.Chat, tids));
            LoadMyMessageOptions(responseOptions, myMessageOptions, tids);
        }

        private List<int> currentDialogueOptions;

        private void LoadMyMessageOptions(List<int> responseOptions, List<int> myMessageOptions, Dictionary<int, string> tids)
        {
            currentDialogueOptions = new List<int>();            
            messagesDropdown.ClearOptions();
            
            var options = new List<TMP_Dropdown.OptionData>();
            foreach (var message in responseOptions)
            {
                var option = new TMP_Dropdown.OptionData(tids[message]);
                options.Add(option);
                currentDialogueOptions.Add(message);
            }
            foreach (var message in myMessageOptions)
            {
                var option = new TMP_Dropdown.OptionData(tids[message]);
                options.Add(option);                
                currentDialogueOptions.Add(message);
            }
            messagesDropdown.AddOptions(options);
        }

        public void OnSendMessageClick()
        {
            var selectedDialogeId = currentDialogueOptions[messagesDropdown.value];
            onSendMessage(friendFromCurrentChat, selectedDialogeId);            
        }

        private IEnumerator LoadMessages(List<WappMessage> friendChat, Dictionary<int, string> tids)
        {
            foreach (var message in friendChat)
            {
                if (message.MessageID == -1)
                {
                    var item = Instantiate(daySeparatorPrefab, chatContainer);
                    yield return new WaitForEndOfFrame();                    
                }
                else
                {
                    var item = Instantiate(message.From == WappMessage.MessageSender.ME ? userMessageChatPrefab : firendMessageChatPrefab, chatContainer);
                    item.Initialize(message, tids);
                    yield return new WaitForEndOfFrame();
                    item.FixThingsUp();
                    yield return new WaitForEndOfFrame();                    
                }                
            }
            chatScroll.normalizedPosition = new Vector2(0, 0);
        }

        public void RefreshFriendButton(WappFriend friend)
        {
            chatWindow.SetActive(false);
            friendsList[friend.friendID].RefreshData(friend);
        }
    }
}