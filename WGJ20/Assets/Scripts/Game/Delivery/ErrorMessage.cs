﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ErrorMessage : MonoBehaviour
{
    public static ErrorMessage Instance { get; private set; }

    [SerializeField] private Text errorText;
    
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    public void ShowMessage(string msg)
    {
        StopAllCoroutines();
        
        gameObject.SetActive(true);
        errorText.text = msg;
        
        StartCoroutine(HideInAWhile());
    }

    private IEnumerator HideInAWhile()
    {
        yield return new WaitForSeconds(3.5f);
        gameObject.SetActive(false);
    }
}
