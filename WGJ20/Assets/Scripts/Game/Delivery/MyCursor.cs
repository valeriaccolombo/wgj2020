﻿using UnityEngine;
using UnityEngine.UI;

public class MyCursor : MonoBehaviour
{
    [SerializeField] private GameObject handWithGloves;
    [SerializeField] private GameObject handNoGloves;
    [SerializeField] private GameObject comb;
    [SerializeField] private GameObject cream;
    [SerializeField] private GameObject heat;
    [SerializeField] private GameObject foil;
    [SerializeField] private GameObject tint;

    [SerializeField] private GameObject pincelDecoObj;
    [SerializeField] private GameObject pincelTintObj;
    [SerializeField] private Image tintColor;

    [SerializeField] private Configurations configurations;
    
    private void Awake()
    {
        Cursor.visible = false;
        handNoGloves.SetActive(true);
    }

    public void UpdateView(Activity currentActivity, bool isWearingGloves, int currentTintColorCode)
    {
        handWithGloves.SetActive(false);
        handNoGloves.SetActive(false);
        comb.SetActive(false);
        cream.SetActive(false);
        heat.SetActive(false);
        foil.SetActive(false);
        tint.SetActive(false);
        
        pincelDecoObj.SetActive(true);
        pincelTintObj.SetActive(true);

        
        switch (currentActivity)
        {
            case Activity.Comb:
                comb.SetActive(true);
                break;
            case Activity.Cream:
                cream.SetActive(true);
                break;
            case Activity.ApplyHeat:
                heat.SetActive(true);
                break;
            case Activity.AddRemoveFoil:
                foil.SetActive(true);
                break;
            case Activity.Tint:
                tint.SetActive(true);
                pincelTintObj.SetActive(false);
                tintColor.color = configurations.GetTintColorByCode(currentTintColorCode);
                break;
            case Activity.ApplyDecolorant20:
            case Activity.ApplyDecolorant30:
            case Activity.ApplyDecolorant40:
                tint.SetActive(true);
                pincelDecoObj.SetActive(false);
                tintColor.color = Color.cyan;
                break;
            default:
                handWithGloves.SetActive(isWearingGloves);
                handNoGloves.SetActive(!isWearingGloves);
                break;
        }
    }
    
    private void Update()
    {
        var pointerPosition = Input.mousePosition;
        transform.position = pointerPosition;
    }
}
