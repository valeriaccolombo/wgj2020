using UnityEngine;
using UnityEngine.UI;

namespace Game.Delivery
{
    public class WappIconView : MonoBehaviour
    {
        [SerializeField] private Text notisCounter;
        [SerializeField] private GameObject notisBadge;

        private void Awake()
        {
            notisCounter.text = "";            
            notisBadge.SetActive(false);
        }
        
        public void RefreshUnreadNotis(int unreadNotis)
        {
            notisCounter.text = unreadNotis.ToString();
            notisBadge.SetActive(unreadNotis > 0);
        }

        private Animator animator;
        public void Animate(string animationTrigger)
        {
            if (animator == null)
                animator = GetComponent<Animator>();
            
            animator.SetTrigger(animationTrigger);
        }
    }
}