﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ClockView : MonoBehaviour
{    
    [SerializeField] private Text dayText;
    [SerializeField] private Text timeText;

    private ClockPresenter presenter;
    private SoundManager soundManager;

    public void Initialize(ClockPresenter presenter, SoundManager soundManager)
    {
        this.presenter = presenter;
        this.soundManager = soundManager;
    }

    public void UpdateTime(DateTime currentTime)
    {
        dayText.text = currentTime.DayOfWeek.ToLocalizedString().ToUpper();
        timeText.text = string.Format("{0}:{1} {2}", currentTime.Hour.ToString("00"), currentTime.Minute.ToString("00"), currentTime.Hour >= 12 ? "PM" : "AM");
    }
}
