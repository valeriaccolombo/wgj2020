using UnityEngine;
using UnityEngine.UI;

public class TintColorToggle : MonoBehaviour
{
    [SerializeField] private Image colorBkg;
    private Toggle toggle;
    private int colorCode;

    public void Initialize(Configurations hairConfiguration)
    {
        toggle = GetComponent<Toggle>();
        colorCode = int.Parse(gameObject.name.Replace("Color", ""));
        colorBkg.color = hairConfiguration.GetTintColorByCode(colorCode);
    }

    public int ColorCode
    {
        get { return colorCode; }
    }

    public bool IsSelected()
    {
        return toggle.isOn;
    }
}
