﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameView : MonoBehaviour
{
    [SerializeField] private MyCursor cursor;
    [SerializeField] private Configurations configurations;
    
    [SerializeField] private Button btnDecolorate20;
    [SerializeField] private Button btnDecolorate30;
    [SerializeField] private Button btnDecolorate40;
    [SerializeField] private Button btnAddRemoveFoil;
    [SerializeField] private Button btnApplyHeat;
    [SerializeField] private Button btnComb;
    [SerializeField] private Button btnAddCream;
    [SerializeField] private Button btnWashAll;
    [SerializeField] private Button btnGoToColor;
    [SerializeField] private Button btnFinishGame;

    [SerializeField] private Toggle GlovesToggle;

    [SerializeField] private List<TintColorToggle> colorToggles;

    [SerializeField] private GameObject decolorationOptions;
    [SerializeField] private GameObject tintOptions;
        
    [SerializeField] private GameObject mouthsuperhappy;
    [SerializeField] private GameObject mouthneutral;
    [SerializeField] private GameObject mouthhorror;

    [SerializeField] private GameObject decolorator20Outline;
    [SerializeField] private GameObject decolorator30Outline;
    [SerializeField] private GameObject decolorator40Outline;
    [SerializeField] private GameObject decoloratorOutline;
    [SerializeField] private GameObject foilOutline;
    [SerializeField] private GameObject heatOutline;
    [SerializeField] private GameObject combOutline;
    [SerializeField] private GameObject creamOutline;

    [SerializeField] private Image TintureTarroColor;
    [SerializeField] private Image TinturePincelColor;

    private GamePresenter presenter;
    private SoundManager soundManager;

    private void Awake()
    {
        btnDecolorate20.onClick.AddListener(() => { SelectActivity(Activity.ApplyDecolorant20); });
        btnDecolorate30.onClick.AddListener(() => { SelectActivity(Activity.ApplyDecolorant30); });
        btnDecolorate40.onClick.AddListener(() => { SelectActivity(Activity.ApplyDecolorant40); });
        btnAddRemoveFoil.onClick.AddListener(() => { SelectActivity(Activity.AddRemoveFoil); });
        btnApplyHeat.onClick.AddListener(() => { SelectActivity(Activity.ApplyHeat); });
        btnComb.onClick.AddListener(() => { SelectActivity(Activity.Comb); });
        btnAddCream.onClick.AddListener(() => { SelectActivity(Activity.Cream); });
        btnGoToColor.onClick.AddListener(GoToColor);
        btnWashAll.onClick.AddListener(WashHead);
        
        
        decolorationOptions.SetActive(true);
        tintOptions.SetActive(false);
    }

    private void OnGlovesToggleValueChanged(bool arg)
    {
        presenter.OnGlovesStatusChange(GlovesToggle.isOn);
        cursor.UpdateView(presenter.CurrentActivity, presenter.IsWearingGloves, presenter.CurrentTintColorCode());
    }

    public void Initialize(GamePresenter presenter, SoundManager soundManager, Configurations hairConfigurations)
    {
        this.presenter = presenter;
        this.soundManager = soundManager;

        btnFinishGame.onClick.AddListener(BtnFinishGameClick);
        
        GlovesToggle.isOn = false;
        GlovesToggle.onValueChanged.AddListener(OnGlovesToggleValueChanged);
        
        colorToggles.ForEach(toggle => toggle.Initialize(hairConfigurations));
        
        UpdateMouthByHealth(false, 100);
        UpdateActivityStatus();
    }

    private void BtnFinishGameClick()
    {
        FullscreenMessage.Instance.Show("¿Listo? ¿Estoy segura de que no quiero hacer nada mas?", presenter.FinishGame,
            () => { });        
    }

    public void OnTintureColorChange()
    {
        presenter.TrySelectActivity(Activity.Tint);
        UpdateActivityStatus();
    }
    
    private void SelectActivity(Activity activity)
    {
        presenter.TrySelectActivity(activity);        
    }

    private void WashHead()
    {
        if(presenter.CurrentActivity != Activity.Tint)
            presenter.TrySelectActivity(Activity.Iddle);
        
        presenter.TryWashHead();
    }

    private void GoToColor()
    {
        presenter.TryGoToColor();
    }

    public int SelectedTintColorCode()
    {
        foreach (var colorToggle in colorToggles)
        {
            if (colorToggle.IsSelected())
                return colorToggle.ColorCode;
        }

        return 0;
    }

    public void ShowTintOptions()
    {
        decolorationOptions.SetActive(false);
        tintOptions.SetActive(true);
    }

    public void BlockGlovesToggle()
    {
        GlovesToggle.interactable = false;
    }
    
    public void UnblockGlovesToggle()
    {
        GlovesToggle.interactable = true;        
    }

    public void TakeScreenshot(int points)
    {
        //ClosePhone
        GameObject.Find("Communication").SetActive(false);

        //ChangeClothes

        //Smileeeee
        UpdateMouthByPoints(points);

        FullscreenMessage.Instance.Close();

        StartCoroutine(WaitAndTakeScreenshot());
    }
    
    private IEnumerator WaitAndTakeScreenshot()
    {
        yield return new WaitForEndOfFrame();
        
        FullscreenMessage.Instance.Close();

        yield return new WaitForEndOfFrame();
       
        presenter.OnFinalScreenshotTaken();
    }

    public void UpdateMouthByPoints(int points)
    {
        mouthhorror.SetActive(points <= 3);
        mouthsuperhappy.SetActive(points > 3 && points < 7);
        mouthneutral.SetActive(points >= 7);        
    }
    
    public void UpdateMouthByHealth(bool anyHairDead, float meanHealth)
    {
        mouthhorror.SetActive(anyHairDead);
        mouthsuperhappy.SetActive(!anyHairDead && meanHealth >= 70);
        mouthneutral.SetActive(!anyHairDead && meanHealth < 70);
    }

    public void UpdateActivityStatus()
    {
        if (presenter == null)
            return;
        
        decolorator20Outline.SetActive(presenter.CurrentActivity == Activity.ApplyDecolorant20);
        decolorator30Outline.SetActive(presenter.CurrentActivity == Activity.ApplyDecolorant30);
        decolorator40Outline.SetActive(presenter.CurrentActivity == Activity.ApplyDecolorant40);
        decoloratorOutline.SetActive(presenter.CurrentActivity == Activity.ApplyDecolorant20 ||
                                     presenter.CurrentActivity == Activity.ApplyDecolorant30 ||
                                     presenter.CurrentActivity == Activity.ApplyDecolorant40);
        foilOutline.SetActive(presenter.CurrentActivity == Activity.AddRemoveFoil);
        heatOutline.SetActive(presenter.CurrentActivity == Activity.ApplyHeat);
        combOutline.SetActive(presenter.CurrentActivity == Activity.Comb);
        creamOutline.SetActive(presenter.CurrentActivity == Activity.Cream);

        TintureTarroColor.color = configurations.GetTintColorByCode(presenter.CurrentTintColorCode());
        TinturePincelColor.color = configurations.GetTintColorByCode(presenter.CurrentTintColorCode());

        cursor.UpdateView(presenter.CurrentActivity, presenter.IsWearingGloves, presenter.CurrentTintColorCode());
    }

    public void PlaySfx(string sfx)
    {
        Debug.Log("Play: " + sfx);
        soundManager.PlaySfx(sfx);
    }

    [SerializeField] private Animator bathroomAnimator;
    public void Animate(string animationTrigger)
    {
        bathroomAnimator.SetTrigger(animationTrigger);
    }
}
