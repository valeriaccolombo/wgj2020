using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WappChatMessageView : MonoBehaviour
{
    [SerializeField] private TMP_Text messageText;
    [SerializeField] private Image messageBkg;

    public void Initialize(WappMessage message, Dictionary<int, string> tids)
    {
        var msg = tids[message.MessageID];
        while(msg.Contains("/"))
            msg = msg.Replace("/", "\n \n");
            
        messageText.text = msg;
    }
    
    public void FixThingsUp()
    {
        var padre = transform.GetComponent<RectTransform>().sizeDelta;
        var texto = messageText.GetComponent<RectTransform>().sizeDelta;    
        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(padre.x, texto.y+6);
    }
}
