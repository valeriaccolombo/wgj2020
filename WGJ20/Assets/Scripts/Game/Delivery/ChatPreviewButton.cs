﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatPreviewButton : MonoBehaviour
{
    [SerializeField] private TMP_Text friendNameText;
    [SerializeField] private TMP_Text msgPreviewText;
    [SerializeField] private GameObject badge;
    [SerializeField] private TMP_Text badgeText;
    
    public void Initialize(WappFriend friend)
    {
        friendNameText.text = friend.name;
        RefreshData(friend);
    }

    public Button ButtonComponent {
        get { return GetComponent<Button>(); }
    }

    public void RefreshData(WappFriend friend)
    {
        badge.SetActive(friend.UnreadNotis > 0);
        badgeText.text = friend.UnreadNotis.ToString();
        msgPreviewText.text = friend.LastMessagePreview();        
    }
}
