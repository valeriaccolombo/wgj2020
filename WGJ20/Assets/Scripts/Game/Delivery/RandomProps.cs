﻿using UnityEngine;

public class RandomProps : MonoBehaviour
{
    [SerializeField] private GameObject obj1;
    [SerializeField] private GameObject obj2;
    [SerializeField] private GameObject obj3;
    
    private void Awake()
    {
        var random = Random.Range(1, 4);
        
        obj1.SetActive(random == 1);
        obj2.SetActive(random == 2);
        obj3.SetActive(random == 3);
    }
}
