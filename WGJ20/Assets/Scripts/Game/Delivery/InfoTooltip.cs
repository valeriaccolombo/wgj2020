﻿using UnityEngine;
using UnityEngine.UI;

public class InfoTooltip : MonoBehaviour
{
    public static InfoTooltip Instance { get; private set; }

    [SerializeField] private Text infoText;
    
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        var pointerPosition = Input.mousePosition;
        transform.position = pointerPosition;
    }

    public void Show(string msg)
    {
        infoText.text = msg;
        gameObject.SetActive(true);
    }

    public void Hide(string msg)
    {
        if(msg == infoText.text)
            gameObject.SetActive(false);
    }
}
