using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonWithTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private string TooltipText;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        InfoTooltip.Instance.Show(TooltipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        InfoTooltip.Instance.Hide(TooltipText);        
    }
}
