﻿using UnityEngine;
using UnityEngine.UI;

public class DecolorantOptions : MonoBehaviour
{
    [SerializeField] private GameObject volumesOptions;    
    [SerializeField] private Button Ouside;
    [SerializeField] private Button Vols20;
    [SerializeField] private Button Vols30;
    [SerializeField] private Button Vols40;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(ClickedDecolorant);
        
        volumesOptions.SetActive(false);
        
        Ouside.onClick.AddListener(ClickedOutside);
        Vols20.onClick.AddListener(ClickedAnOption);
        Vols30.onClick.AddListener(ClickedAnOption);
        Vols40.onClick.AddListener(ClickedAnOption);
    }

    private void ClickedDecolorant()
    {
        volumesOptions.SetActive(true);
    }

    private void ClickedAnOption()
    {
        volumesOptions.SetActive(false);
    }

    private void ClickedOutside()
    {
        volumesOptions.SetActive(false);
    }
}
