﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HairPieceView : MonoBehaviour
{
    private Image image;
    [SerializeField] private GameObject decolorant;
    [SerializeField] private GameObject foil;
    [SerializeField] private Text healthText;
    [SerializeField] private Image tinture;

    [SerializeField] private List<GameObject> piecesDependingOnIt;

    private HairPiecePresenter presenter;
    private SoundManager soundManager;
    
    private void Awake()
    {
        image = GetComponent<Image>();

        var hitArea = GetComponentInChildren<Button>();
        hitArea.onClick.AddListener(OnClick);

        decolorant.SetActive(false);
        foil.SetActive(false);
        tinture.gameObject.SetActive(false);
    }

    public void Initialize(HairPiecePresenter presenter, SoundManager soundManager)
    {
        this.presenter = presenter;
        this.soundManager = soundManager;
    }
    
    private void OnClick()
    {
        presenter.TryDoAction();
    }

    public void ShowFinalColor(Color finalColor, int health)
    {
        UpdateStatus(finalColor, health, false, false, -1, "wash");
    }
    
    public void UpdateStatus(Color color, int health, bool hasDecolorant, bool hasFoil, int currentTint, string animation = "", string sound = "")
    {
        if (!string.IsNullOrEmpty(sound) && soundManager != null)
        {
            Debug.Log("Play: " + sound);
            soundManager.PlaySfx(sound);           
        }

        if(animation == "wash")        
            StartCoroutine(UpdateStatus2(2, color, health, hasDecolorant, hasFoil, currentTint));
        else
            UpdateStatus3(color, health, hasDecolorant, hasFoil, currentTint);
    }

    private IEnumerator UpdateStatus2(float delay, Color color, int health, bool hasDecolorant, bool hasFoil, int currentTint)
    {
        yield return new WaitForSeconds(delay);

        UpdateStatus3(color, health, hasDecolorant, hasFoil, currentTint);
    }

    private void UpdateStatus3(Color color, int health, bool hasDecolorant, bool hasFoil, int currentTint)
    {
        if (health <= 0)
        {
            if (gameObject.activeSelf)
            {
                //Significa que acaba de morir en este momento!
            }

            gameObject.SetActive(false);
            foreach (var piece in piecesDependingOnIt)
            {
                piece.GetComponent<HairPieceView>().FallBecauseParentFell();
            }
        }

        healthText.text = ""; //"Vida: " + health;
        decolorant.SetActive(hasDecolorant);
        foil.SetActive(hasFoil);

        tinture.gameObject.SetActive(currentTint != -1);
        if (currentTint != -1)
        {
            var tintureColor = presenter.ColorForTintureCode(currentTint);
            tintureColor.a = 0.5f;
            tinture.color = tintureColor;
        }

        image.color = color;
    }

    private void FallBecauseParentFell()
    {
        presenter.FallBecauseParentFell();
        gameObject.SetActive(false);
    }


    public void ShowFloatingErrorMessage(string message)
    {
        ErrorMessage.Instance.ShowMessage(message);
    }
}
