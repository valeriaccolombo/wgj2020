﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FullscreenMessage : MonoBehaviour
{
    public static FullscreenMessage Instance { get; private set; }

    [SerializeField] private TMP_Text msgText;
    [SerializeField] private Button buttonOk;
    [SerializeField] private Button buttonEsc;

    private Action callbackOk;
    private Action callbackError;
    
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    public void Show(string message, Action callbackOk, Action callbackError = null)
    {
        gameObject.SetActive(true);
        
        this.callbackOk = callbackOk;
        this.callbackError = callbackError;
        
        msgText.text = message;
        
        buttonOk.onClick.AddListener(OnClickOK);

        buttonEsc.gameObject.SetActive(false);
        if (callbackError != null)
        {
            buttonEsc.onClick.AddListener(OnClickError);            
            buttonEsc.gameObject.SetActive(true);
        }
    }

    private void OnClickOK()
    {
        callbackOk();
        Close();
    }
    
    private void OnClickError()
    {
        callbackError();
        Close();
    }

    public void Close()
    {
        this.callbackOk = null;
        this.callbackError = null;
     
        buttonOk.onClick.RemoveAllListeners();
        buttonEsc.onClick.RemoveAllListeners();
        
        gameObject.SetActive(false);
    }
}
