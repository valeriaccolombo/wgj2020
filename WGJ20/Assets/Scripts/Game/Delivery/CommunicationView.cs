﻿using Game.Delivery;
using UnityEngine;

public class CommunicationView : MonoBehaviour
{
    private CommunicationPresenter presenter;
    [SerializeField] private SoundManager soundManager;
    [SerializeField] private Animator eyesAnimator;

    public WappIconView WappIconView;
    public WappWindowView WappWindowView;
    
    public void Initialize(CommunicationPresenter presenter)
    {
        this.presenter = presenter;
    }

    public void OnClickWappIcon()
    {
        presenter.TryOpenPhone();
    }
    
    public void OnClickClosePhone()
    {
        WappWindowView.CloseFriendChat();
        presenter.TryClosePhone();
    }

    public void OnMessageReceived()
    {
        eyesAnimator.SetTrigger("LookAtPhone");
        soundManager.PlaySfx("notification");
        WappIconView.Animate("ring");
    }
}
