public static class TimeConfigurations
{
    public static double TIME_TO_MINUTES_FACTOR = 1;
    
    public static double AMinuteInSeconds {
        get { return 1/TIME_TO_MINUTES_FACTOR; }
    }
    
    public static double SecondsToMinutes(int seconds)
    {
        return seconds * TIME_TO_MINUTES_FACTOR;
    }
}
