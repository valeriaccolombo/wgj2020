public class DummyWappFriend : WappFriend
{
    private string previewMessage;
    public DummyWappFriend(int friendID, string name, string previewMessage) : base(friendID)
    {
        this.name = name;
        this.previewMessage = previewMessage;
        UnreadNotis = 0;
    }

    public override string LastMessagePreview()
    {
        return previewMessage;
    }
}