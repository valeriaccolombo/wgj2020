public class WappMessage
{
    public enum MessageSender { ME, FRIEND }

    public MessageSender From { get; private set; }
    public int MessageID { get; private set; }

    public WappMessage(int messageId, MessageSender from)
    {
        MessageID = messageId;
        From = from;
    }
}