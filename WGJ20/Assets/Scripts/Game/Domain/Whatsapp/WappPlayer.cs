using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WappPlayer
{
    public int[] QuestionsAvailable { get; private set; }
    private Dictionary<int, int[]> ansersAvailable;    
    
    public WappPlayer(TextAsset playerFile, Dictionary<int, string> tids)
    {
        var lines = playerFile.text.Split("\n"[0]);

        var questionsAvailableData = (lines[0].Replace("\\r", "").Trim()).Split(',');
        QuestionsAvailable = questionsAvailableData[1].Split('-').Select(a => int.Parse(a)).ToArray();
        
        ansersAvailable = new Dictionary<int, int[]>();
        for (var i = 2; i < lines.Length; i++)
        {
            var lineData = (lines[i].Replace("\\r", "").Trim()).Split(',');
            ansersAvailable.Add(int.Parse(lineData[0]),
                lineData[1].Split('-').Select(a => int.Parse(a)).ToArray());
        }
    }

    public List<int> AnsersAvailableFor(WappMessage message)
    {
        if (message.From == WappMessage.MessageSender.FRIEND && ansersAvailable.ContainsKey(message.MessageID))
        {
            return ansersAvailable[message.MessageID].ToList();
        }
        else
        {
            return new List<int>(); 
        }
    }
}
