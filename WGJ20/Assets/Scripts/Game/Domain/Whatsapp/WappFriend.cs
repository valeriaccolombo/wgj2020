using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class WappFriend
{
    public int friendID { get; private set; }
    private int minAnswerTime;
    public string name { get; protected set; }
    private int maxAnswerTime;
    private int minMsgIddleTime;
    private int maxMsgIddleTime;
    private int[] iddleMessagesAvailable;
    private Dictionary<int, int[]> anwsersAvailable;

    private CommunicationView coroutineRunner;
    private readonly Action onMessageSent;
    private readonly Dictionary<int, string> tids;
    private Coroutine currentCoroutine;

    public List<WappMessage> Chat { get; protected set; }
    public int UnreadNotis { get; protected set; }

    private int lastIddleMessageSent;
    
    public WappFriend(int friendID)
    {
        //"No usar este constructor!!!"
    }

    public WappFriend(int friendID, TextAsset friendFile, CommunicationView coroutineRunner,
        int initialWaitSecondsForChat, Action onMessageSent, Dictionary<int, string> tids)
    {
        this.friendID = friendID;
        this.coroutineRunner = coroutineRunner;
        this.onMessageSent = onMessageSent;
        this.tids = tids;

        var lines = friendFile.text.Split("\n"[0]);

        name = lines[0].Replace("\\r", "").Trim().Split(',')[1];
        minAnswerTime = int.Parse(lines[1].Replace("\\r", "").Trim().Split(',')[1]);
        maxAnswerTime = int.Parse(lines[2].Replace("\\r", "").Trim().Split(',')[1]);
        minMsgIddleTime = int.Parse(lines[3].Replace("\\r", "").Trim().Split(',')[1]);
        maxMsgIddleTime = int.Parse(lines[4].Replace("\\r", "").Trim().Split(',')[1]);
        
        var iddleMessagesAvailableData = (lines[6].Replace("\\r", "").Trim()).Split(',')[1];
        iddleMessagesAvailable = iddleMessagesAvailableData.Split('-').Select(a => int.Parse(a)).ToArray();
        
        anwsersAvailable = new Dictionary<int, int[]>();
        for (var i = 8; i < lines.Length; i++)
        {
            var lineData = (lines[i].Replace("\\r", "").Trim()).Split(',');
            anwsersAvailable.Add(int.Parse(lineData[0]),
                lineData[1].Split('-').Select(a => int.Parse(a)).ToArray());
        }

        var msgsInHistoryLineData = lines[5].Replace("\\r", "").Trim().Split(',')[1];
        var msgsInHistory = msgsInHistoryLineData.Split('-').Select(a => int.Parse(a)).ToArray();
        Chat = new List<WappMessage>();
        foreach (var msgInHistory in msgsInHistory)
        {
            Chat.Add(new WappMessage(msgInHistory, WappMessage.MessageSender.FRIEND));            
        }
        Chat.Add(new WappMessage(-1, WappMessage.MessageSender.FRIEND));

        lastIddleMessageSent = 0;
        currentCoroutine = this.coroutineRunner.StartCoroutine(StartMessaging(initialWaitSecondsForChat));
    }

    private void SendMessage(int messageId)
    {
        if (messageId == 12) //ignorar
            return;
        
        Chat.Add(new WappMessage(messageId, WappMessage.MessageSender.FRIEND));
        UnreadNotis++;
        onMessageSent();
    }

    public void ReceiveMessage(int messageId)
    {
        Chat.Add(new WappMessage(messageId, WappMessage.MessageSender.ME));
        
        coroutineRunner.StopCoroutine(currentCoroutine);
        currentCoroutine = coroutineRunner.StartCoroutine(SendResponseFor(messageId));
    }
    
    public IEnumerator StartMessaging(int initialWaitSecondsForChat)
    {
        yield return new WaitForSeconds(initialWaitSecondsForChat + Random.Range(minMsgIddleTime, maxMsgIddleTime));

        while (lastIddleMessageSent < iddleMessagesAvailable.Length)
        {
            SendMessage(iddleMessagesAvailable[lastIddleMessageSent]);
            lastIddleMessageSent++;
            yield return new WaitForSeconds(Random.Range(minMsgIddleTime, maxMsgIddleTime));                
        }
    }

    public IEnumerator SendResponseFor(int messageId)
    {
        yield return new WaitForSeconds(Random.Range(minAnswerTime, maxAnswerTime));

        if (anwsersAvailable.ContainsKey(messageId))
        {
            SendMessage(anwsersAvailable[messageId].PickRandom());            
        }

        currentCoroutine = coroutineRunner.StartCoroutine(StartMessaging(0));
    }

    public virtual string LastMessagePreview()
    {
        var last_chat = Chat[Chat.Count - 1];
        if (last_chat.MessageID == -1)
            last_chat = Chat[Chat.Count - 2];

        return tids[last_chat.MessageID];
    }

    public void MarkAsRead()
    {
        UnreadNotis = 0;
    }
}
