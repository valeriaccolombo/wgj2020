﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Configurations : MonoBehaviour
{
    [SerializeField] private List<ColorData> hairColors;
    [SerializeField] private List<ColorData> tintureColors;    
    [SerializeField] private List<ColorData> resultingColors;    
    
    [SerializeField] private int combDamage = 3;
    public int CreamPower = 2;
        
    private Dictionary<string, DecolorationResult> decolorationConfig;
    private Dictionary<string, Color> tintureConfig;
    private Dictionary<string, int> tintureColotPoints;

    private void Awake()
    {
        LoadDecolorationData();
        LoadTintureData();
    }

    private void LoadDecolorationData()
    {
        decolorationConfig = new Dictionary<string, DecolorationResult>();
        
        var decolorationFile = Resources.Load<TextAsset>("Data/DecolorationConfigs");
        var lines = decolorationFile.text.Split("\n"[0]);

        for (var i = 1; i < lines.Length; i++)
        {
            var lineData = (lines[i].Replace("\\r", "").Trim()).Split(',');

            var code = string.Format("{0}-{1}-{2}", lineData[0], lineData[1], lineData[2]);
            var result = new DecolorationResult(int.Parse(lineData[4]), int.Parse(lineData[3]));
            decolorationConfig.Add(code, result);
        }
    }
    
    private void LoadTintureData()
    {
        tintureConfig = new Dictionary<string, Color>();
        tintureColotPoints = new Dictionary<string, int>();
        
        var tinturesFile = Resources.Load<TextAsset>("Data/TintureConfigs");
        var lines = tinturesFile.text.Split("\n"[0]);

        for (var i = 1; i < lines.Length; i++)
        {
            var lineData = (lines[i].Replace("\\r", "").Trim()).Split(',');

            var colorCode = int.Parse(lineData[2]);
            var code = string.Format("{0}-{1}", lineData[0], lineData[1]);
            var points = int.Parse(lineData[3]);
            
            tintureConfig.Add(code, resultingColors.Find(data => data.Code == colorCode).Color);
            tintureColotPoints.Add(code, points);
        }
    }

    public int GetTimeCode(int seconds, int codeVols)
    {
        var timeToMinutes = TimeConfigurations.SecondsToMinutes(seconds);

        switch (codeVols)
        {
            case 0: //20vols
                if (timeToMinutes <= 45)
                {
                    return 0;
                }
        
                if (timeToMinutes <= 75)
                {
                    return 1;
                }

                return 2;
                break;
            case 1: //30vols
                if (timeToMinutes <= 30)
                {
                    return 0;
                }
        
                if (timeToMinutes <= 60)
                {
                    return 1;
                }

                return 2;
            default: //40vols
                if (timeToMinutes <= 20)
                {
                    return 0;
                }
        
                if (timeToMinutes <= 45)
                {
                    return 1;
                }

                return 2;
        }
    }

    public int GetHeadCode(bool usedHeat, bool usedFoil)
    {
        if (!usedHeat)
            return 0;

        if (!usedFoil)
            return 1;

        return 2;
    }

    private int GetCombDamage()
    {
        return combDamage;
    }
  
    public DecolorationResult GetDecolorationResultByCode(int baseColor, int timeCode, int heatCode, int volumesUsed, bool hasBeenCombed)
    {
        var result = decolorationConfig[string.Format("{0}-{1}-{2}", baseColor, timeCode, heatCode)];

        if (hasBeenCombed)
            result.LifeDamage += GetCombDamage();

        if (volumesUsed == 0)
            result.LifeDamage *= 0.9;
        
        if (volumesUsed == 2)
            result.LifeDamage *= 1.1;
        
        return result;
    }
    
    public Color GetTintureResult(int baseColor, int tintureColor)
    {
        return tintureConfig[string.Format("{0}-{1}", baseColor, tintureColor)];
    }

    public int GetTintureResultPoints(int baseColor, int tintureColor)
    {
        return tintureColotPoints[string.Format("{0}-{1}", baseColor, tintureColor)];
    }
        
    public Color GetHairColorByCode(int code)
    {
        return hairColors.Find(data => data.Code == code).Color;
    }

    public Color GetTintColorByCode(int code)
    {
        return tintureColors.Find(data => data.Code == code).Color;
    }
}

[Serializable]
public class ColorData
{
    public int Code;
    public Color Color;
}

public class DecolorationResult
{
    public double LifeDamage;
    public int ResultColor;

    public DecolorationResult(double lifeDamage, int resultColor)
    {
        LifeDamage = lifeDamage;
        ResultColor = resultColor;
    }
}


