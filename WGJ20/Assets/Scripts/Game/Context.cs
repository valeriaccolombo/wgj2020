using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Context : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    [SerializeField] private GameView gameView;
    [SerializeField] private ClockView clockView;
    [SerializeField] private CommunicationView communicationView;
    [SerializeField] private List<HairPieceView> hairPieceViews;
    [SerializeField] private Configurations hairConfigurations;
    
    [SerializeField] private Transform minitahOrig;
    [SerializeField] private Photo startPhoto;
    [SerializeField] private Photo finalPhoto;
    
    private void Start()
    {
        var introMsg =
            "Tengo ganas de hacerme algo loco en el pelo. Pero mañana me tengo que levantar temprano, asi que voy a intentar no quedarme hasta muy tarde con esto";
        FullscreenMessage.Instance.Show(introMsg, StartGame);
    }

    private void StartGame()
    {
        InMemoryData.Instance.InitialPhoto = startPhoto;
        InMemoryData.Instance.ResultingPhoto = finalPhoto;
        
        var intialHealt = RandomizeHairInitialHealt();
        var intialColor = RandomizeHairInitialColor();
        
        var gamePresenter = new GamePresenter(gameView, finalPhoto, minitahOrig);
        gameView.Initialize(gamePresenter, soundManager, hairConfigurations);
        
        var clockPresenter = new ClockPresenter(clockView, gamePresenter);
        clockView.Initialize(clockPresenter, soundManager);
        
        var hairPiecePresenters = new List<HairPiecePresenter>();
        foreach (var hairPieceView in hairPieceViews)
        {
            var hairPiecePresenter = new HairPiecePresenter(hairPieceView, gamePresenter, hairConfigurations, intialHealt, intialColor);
            hairPieceView.Initialize(hairPiecePresenter, soundManager);
            hairPiecePresenters.Add(hairPiecePresenter);
        }

        gamePresenter.SetHairPieces(hairPiecePresenters);
        
        var communicationPresenter = new CommunicationPresenter(communicationView, gamePresenter);
        communicationView.Initialize(communicationPresenter);

        StartCoroutine(TakeStartingPhoto());
    }

    private IEnumerator TakeStartingPhoto()
    {
        yield return new WaitForEndOfFrame();
        startPhoto.CopyFrom(minitahOrig);
    }

    private int RandomizeHairInitialHealt()
    {
        return Random.Range(90, 110);
    }

    private int RandomizeHairInitialColor()
    {
        var baseColors = new int[] {0, 1, 2, 3};
        return baseColors.PickRandom();
    }
}
