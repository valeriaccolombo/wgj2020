using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePresenter
{
    private GameView view;
    private readonly Photo finalPhoto;
    private readonly Transform minitahOrig;
    
    private List<HairPiecePresenter> hairPiecePresenters;

    public Activity CurrentActivity { get; private set; }
    public bool IsWearingGloves { get; private set; }

    public GamePresenter(GameView gameView, Photo finalPhoto, Transform minitahOrig)
    {
        this.view = gameView;
        this.finalPhoto = finalPhoto;
        this.minitahOrig = minitahOrig;
    }

    public void TryWashHead()
    {
        if(!IsWearingGloves)
        {
            ErrorMessage.Instance.ShowMessage("Mmm... no debería hacer nada sin ponerme los guantes");
            return;
        }

        view.Animate("wash");
        view.PlaySfx("wash");
        hairPiecePresenters.ForEach(hairPiecePresenter => hairPiecePresenter.Wash());
        BlockGlovesWhileWorking();
        UpdateStatus();
    }

    public void BlockGlovesWhileWorking()
    {
        view.StartCoroutine(BlockGlovesForSeconds(2));
    }

    private IEnumerator BlockGlovesForSeconds(int seconds)
    {
        view.BlockGlovesToggle();
        yield return new WaitForSeconds(seconds);
        view.UnblockGlovesToggle();
    }

    public void TrySelectActivity(Activity activity)
    {
        if (activity != Activity.Tint && CurrentActivity == activity)
            CurrentActivity = Activity.Iddle;
        else
            CurrentActivity = activity;

        view.UpdateActivityStatus();
    }

    public void SetHairPieces(List<HairPiecePresenter> hairPiecePresenters)
    {
        this.hairPiecePresenters = hairPiecePresenters;
    }

    public void FinishGame()
    {
        InMemoryData.Instance.totalPoints = CalculateScore();
        view.TakeScreenshot(InMemoryData.Instance.totalPoints);
    }
    
    public void OnFinalScreenshotTaken()
    {
        finalPhoto.CopyFrom(minitahOrig);
        SceneManager.LoadScene("EndScreen");
    }

    private int CalculateScore()
    {
        var score = 0;
        foreach (var hairPiece in hairPiecePresenters)
        {
            score += hairPiece.PiecePoints;
        }

        return score / hairPiecePresenters.Count;
    }

    public void TryGoToColor()
    {
        if (hairPiecePresenters.Exists(hairPiecePresenter => !hairPiecePresenter.IsReadyForColor()))
        {
            ErrorMessage.Instance.ShowMessage("Tengo que lavarme la cabeza antes de teñirme!!!");
        }
        else
        {
            FullscreenMessage.Instance.Show("¿Ya terminé con la decoloración? Una vez que pasé a la coloración ya no puedo volver para atras...", 
                () => {
                    CurrentActivity = Activity.Tint;
                    view.ShowTintOptions();
                },
                () => { });
        }
    }

    public int CurrentTintColorCode()
    {
        return view.SelectedTintColorCode();
    }

    public void OnGlovesStatusChange(bool glovesToggleIsOn)
    {
        view.PlaySfx("gloves");
        IsWearingGloves = glovesToggleIsOn;
        if (!IsWearingGloves)
            CurrentActivity = Activity.Iddle;
    }

    public void TimeUp()
    {
        FullscreenMessage.Instance.Show("Bueno, hasta aca llegué, me tengo que ir a dormir si o si porque mañana es un día muy largo", FinishGame);
    }

    public void UpdateStatus()
    {
        if (CurrentActivity != Activity.Tint)
        {
            bool anyHairDead = false;
            float meanHealth = 0;

            foreach (var hair in hairPiecePresenters)
            {
                anyHairDead = anyHairDead || hair.currentHealt <= 0;
                meanHealth += hair.currentHealt;
            }
            meanHealth = meanHealth / hairPiecePresenters.Count;

            view.UpdateMouthByHealth(anyHairDead, meanHealth);            
        }
        else
        {
            view.UpdateMouthByPoints(CalculateScore());
        }
    }
}

public enum Activity
{
    Iddle, Cream, ApplyDecolorant20, ApplyDecolorant30, ApplyDecolorant40, Comb, AddRemoveFoil, ApplyHeat, Tint
}

