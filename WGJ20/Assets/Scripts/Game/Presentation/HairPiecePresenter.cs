using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class HairPiecePresenter
{
    private HairPieceView view;
    private GamePresenter game;
    private Configurations hairConfigurations;

    public int currentHealt { get; private set; }
    public int currentColor { get; private set; }
    
    public int PiecePoints { get; private set; }

    private bool hasDecolorant;
    private bool hasFoil;
    private bool hasBeenCombed;
    private bool hasBeenHeated;
    private int volumesUsed;
    private bool hasUsedCream;
    private int currentTintOn;

    private bool colorDone = false;
    
    public HairPiecePresenter(HairPieceView view, GamePresenter game, Configurations hairConfigurations, int intialHealt, int intialColor)
    {
        this.view = view;
        this.game = game;
        this.hairConfigurations = hairConfigurations;
        this.currentHealt = intialHealt;
        this.currentColor = intialColor;

        PiecePoints = 5; //Pelo al natural: 5 pts
            
        ResetAll();
        UpdateView();
    }

    private void UpdateView(string animation = "", string audio = "")
    {
        if (animation != "")
        {
            game.BlockGlovesWhileWorking();
        }
        view.UpdateStatus(hairConfigurations.GetHairColorByCode(currentColor), currentHealt, hasDecolorant, hasFoil, currentTintOn, animation, audio);
    }

    public void TryDoAction()    
    {
        Debug.Log("try: " + game.CurrentActivity);
        
        if (colorDone)
        {
            view.ShowFloatingErrorMessage("Esta parte ya esta terminada, debería dejarla en paz!!!");
            return;
        }

        if (!game.IsWearingGloves)
        {
            view.ShowFloatingErrorMessage("Mmm... no debería hacer nada sin ponerme los guantes");
            return;            
        }
        
        var animation = "";
        var audio = "";
        switch (game.CurrentActivity)
        {
            case Activity.Comb:
                Comb(out animation, out audio);
                break;
            case Activity.Cream:
                PutCream(out animation, out audio);
                break;
            case Activity.AddRemoveFoil:
                AddOrRemoveFoil(out animation, out audio);
                break;
            case Activity.ApplyHeat:
                ApplyHeat(out animation, out audio);
                break;
            case Activity.ApplyDecolorant20:
                ApplyDecolorant(0, out animation, out audio);
                break;
            case Activity.ApplyDecolorant30:
                ApplyDecolorant(1, out animation, out audio);
                break;
            case Activity.ApplyDecolorant40:
                ApplyDecolorant(2, out animation, out audio);
                break;
            case Activity.Tint:
                ApplyTint(out animation, out audio);
                break;
        }

        game.UpdateStatus();
        UpdateView(animation, audio);
    }

    private void ApplyTint(out string animation, out string audio)
    {
        if (currentTintOn != -1)
        {
            view.ShowFloatingErrorMessage("Esta parte ya tiene tintura!!!");
            animation = "";
            audio = "";
        }
        else
        {
            currentTintOn = game.CurrentTintColorCode();
            animation = string.Format("tint_", currentTintOn);
            audio = Random.Range(0,1) > 0.5f ? "apply" : "apply2";
        }
    }

    private void AddOrRemoveFoil(out string animation, out string audio)
    {
        if (!hasFoil)
        {
            AddFoil(out animation, out audio);
        }
        else
        {
            RemoveFoil(out animation, out audio);
        }
    }

    private void RemoveFoil(out string animation, out string audio)
    {
        if (!hasFoil)
        {
            view.ShowFloatingErrorMessage("No hay nada que sacar aca");
            animation = "";
            audio = "";
        }
        else
        {
            hasFoil = false;
            animation = "remove_foil";
            audio = "foil";
        }
    }

    private void AddFoil(out string animation, out string audio)
    {
        if (!hasDecolorant)
        {
            view.ShowFloatingErrorMessage("Pa' que me voy a poner papel aluminio sin decolorante?");
            animation = "";
            audio = "";
        }
        else if (hasFoil)
        {
            view.ShowFloatingErrorMessage("Ya esta bien envuelta esta parte!!!");
            animation = "";
            audio = "";
        }
        else
        {
            hasFoil = true;
            animation = "apply_foil";
            audio = "foil";
        }
    }
    
    
    private void ApplyHeat(out string animation, out string audio)
    {
        if (!hasDecolorant)
        {
            view.ShowFloatingErrorMessage("Pa' que me voy a dar calor si no me puse decolorante?");
            animation = "";
            audio = "";
        }
        else
        {
            hasBeenHeated = true;
            animation = "apply_heat";
            audio = "heat";
        }
    }


    private void PutCream(out string animation, out string audio)
    {
        if (hasDecolorant)
        {
            view.ShowFloatingErrorMessage("No se puede poner crema arriba del decolorante!!!");
            animation = "";
            audio = "";
        }
        else if (hasUsedCream)
        {
            view.ShowFloatingErrorMessage("A esta parte ya le puse crema una vez, mas milagros no va a hacer");
            animation = "";
            audio = "";
        }
        else
        {
            hasUsedCream = true;
            currentHealt = Mathf.Min(110, currentHealt + hairConfigurations.CreamPower);
            animation = "apply_cream";
            audio = "cream";
        }
    }

    private void Comb(out string animation, out string audio)
    {
        if (hasFoil)
        {
            view.ShowFloatingErrorMessage("No puedo peinar arriba del papel aluminio...");
            animation = "";
            audio = "";
        }
        else if (hasDecolorant)
        {
            hasBeenCombed = true;
            animation = "comb";
            audio = "comb";
        }
        else
        {
            animation = "comb";
            audio = "comb";
        }
    }

    private void ApplyDecolorant(int vols, out string animation, out string audio)
    {
        if (hasDecolorant) 
        {
            view.ShowFloatingErrorMessage("Ya tiene suficiente decolorante esta parte");
            animation = "";
            audio = "";
        }
        else
        {
            volumesUsed = vols;
            hasDecolorant = true;
            StartDecolorationTimer(); 
            animation = "apply_decolorant";
            audio = Random.Range(0,1) > 0.5f ? "apply" : "apply2";
        }
    }

    private DateTime? startedDecolorationTime;
    private void StartDecolorationTimer()
    {
        startedDecolorationTime = DateTime.Now;
    }

    public void Wash()
    {        
        if (colorDone)
        {
            return;
        }
        
        if (currentTintOn == -1)
        {
            if (startedDecolorationTime.HasValue)
            {
                var secondsElapsed = (DateTime.Now - startedDecolorationTime.Value).Seconds;

                var decolorationResult = hairConfigurations.GetDecolorationResultByCode(currentColor,
                    hairConfigurations.GetTimeCode(secondsElapsed, volumesUsed),
                    hairConfigurations.GetHeadCode(hasBeenHeated, hasFoil), volumesUsed, hasBeenCombed);

                currentColor = decolorationResult.ResultColor;
                currentHealt = Math.Max(0, currentHealt + (int) decolorationResult.LifeDamage);

                PiecePoints = currentHealt > 0 ? 4 : 0; //Pelo muerto: 0 puntos, pelo decolorado: 4 puntos
            }

            ResetAll();
            UpdateView("wash");
        }
        else
        {
            var finalColor = hairConfigurations.GetTintureResult(currentColor, currentTintOn);
            //Debug.LogError("tint for: " + currentColor + "-" +currentTintOn +": " + finalColor.ToString());
            PiecePoints = hairConfigurations.GetTintureResultPoints(currentColor, currentTintOn);
            view.ShowFinalColor(finalColor, currentHealt);
            colorDone = true;
        }
    }

    private void ResetAll()
    {
        volumesUsed = 1;
        startedDecolorationTime = null;
        hasDecolorant = false;
        hasFoil = false;
        hasBeenCombed = false;
        hasBeenHeated = false;
        hasUsedCream = false;
        currentTintOn = -1;
    }

    public bool IsReadyForColor()
    {
        return !hasDecolorant;
    }

    public Color ColorForTintureCode(int currentTint)
    {
        return hairConfigurations.GetTintColorByCode(currentTint);
    }

    public void FallBecauseParentFell()
    {
        currentHealt = 0;
        PiecePoints = 0;
    }
}
