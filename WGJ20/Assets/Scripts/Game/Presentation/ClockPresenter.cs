
using System;
using System.Collections;
using UnityEngine;

public class ClockPresenter
{
    private ClockView view;
    private readonly GamePresenter gamePresenter;

    private DateTime currentTime;
    private DateTime limitTime;

    public ClockPresenter(ClockView view, GamePresenter gamePresenter)
    {
        this.view = view;
        this.gamePresenter = gamePresenter;

        currentTime = new DateTime(2020, 8, 16, 11,0,0);
        limitTime = new DateTime(2020, 8, 17, 0,5,0);
        view.UpdateTime(currentTime);

        view.StartCoroutine(PassTheTime());
    }

    private IEnumerator PassTheTime()
    {
        var aMinuteInSeconds = (float)TimeConfigurations.AMinuteInSeconds;

        while (currentTime < limitTime)
        {
            yield return new WaitForSeconds(aMinuteInSeconds);

            currentTime = currentTime.AddMinutes(1);
            view.UpdateTime(currentTime);
        }

        gamePresenter.TimeUp();
    }
}
