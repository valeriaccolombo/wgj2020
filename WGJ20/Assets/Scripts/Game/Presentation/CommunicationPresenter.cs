﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CommunicationPresenter
{
    private CommunicationView view;
    private GamePresenter game;

    private Dictionary<int, string> tids;
    private WappPlayer player;
    private List<WappFriend> friends;
    
    public CommunicationPresenter(CommunicationView view, GamePresenter game)
    {
        this.view = view;
        this.game = game;
        
        view.WappWindowView.gameObject.SetActive(false);

        LoadTIDs();
        friends = new List<WappFriend>();
        friends.Add(CreateFriend(1));
        friends.Add(CreateFriend(2));
        friends.Add(CreateFriend(3));
        friends.Add(CreateFriend(4));
        CreatePlayer();

        view.WappWindowView.LoadFriendsList(friends, OnOpenFriendChat, OnCloseFriendChat, OnSendMessage);
    }

    private void OnSendMessage(WappFriend friend, int dialogeId)
    {
        friend.ReceiveMessage(dialogeId);
        
        var last_chat = friend.Chat[friend.Chat.Count - 1];
        if(last_chat.MessageID == -1)
            last_chat = friend.Chat[friend.Chat.Count - 2];
        
        var responseOptions = player.AnsersAvailableFor(last_chat);
        var messageOptions = player.QuestionsAvailable.ToList();
        if (friend.friendID == 4) //es mama
        {
            //no le hace preguntas de coloracion a mama
            messageOptions = new List<int>() {238,4,8};
        }

        view.WappWindowView.ReloadMessages(friend, responseOptions,messageOptions, tids);
    }

    private void OnOpenFriendChat(WappFriend friend)
    {
        var last_chat = friend.Chat[friend.Chat.Count - 1];
        if(last_chat.MessageID == -1)
            last_chat = friend.Chat[friend.Chat.Count - 2];
        
        var responseOptions = player.AnsersAvailableFor(last_chat);
        var messageOptions = player.QuestionsAvailable.ToList();
        if (friend.friendID == 4) //es mama
        {
            //no le hace preguntas de coloracion a mama
            messageOptions = new List<int>() {238,4,8};
        }
        
        view.WappWindowView.ShowChatForFriend(friend, responseOptions,messageOptions, tids);
        friend.MarkAsRead();
        RefresNotis();        
    }
    
    private void OnCloseFriendChat(WappFriend friend)
    {
        RefresNotis();
        view.WappWindowView.RefreshFriendButton(friend);
    }
    
    private void CreatePlayer()
    {
        var playerFile = Resources.Load<TextAsset>("Data/whatsapp_player");
        player = new WappPlayer(playerFile, tids);
    }

    private WappFriend CreateFriend(int friendID)
    {
        var friendFile = Resources.Load<TextAsset>("Data/whatsapp_friend"+friendID);
        return new WappFriend(friendID, friendFile, view, 5, ReceiveMessage, tids);
    }

    private void ReceiveMessage()
    {
        RefresNotis();
        view.OnMessageReceived();
    }

    private void RefresNotis()
    {
        var unreadNotis = 0;
        foreach (var friend in friends)
        {
            unreadNotis += friend.UnreadNotis;
        }
        view.WappIconView.RefreshUnreadNotis(unreadNotis);
    }

    private void LoadTIDs()
    {
        tids = new Dictionary<int, string>();
        
        var tidsFile = Resources.Load<TextAsset>("Data/whatsapp_texts");
        var lines = tidsFile.text.Split("\n"[0]);

        for (var i = 0; i < lines.Length; i++)
        {
            var lineData = (lines[i].Replace("\\r", "").Trim()).Split(',');
            tids.Add(int.Parse(lineData[0]), ReplaceCharacters(lineData[1]));
        }
    }

    private string ReplaceCharacters(string s)
    {
        while (s.Contains(';'))
        {
            s = s.Replace(';', ',');
        }

        return s;
    }

    public void TryOpenPhone()
    {
        if (game.IsWearingGloves)
        {
            ErrorMessage.Instance.ShowMessage("Debería sacarme los guantes antes de agarrar el teléfono");
            return;
        }
        
        view.WappWindowView.RefreshNotis(friends);
        view.WappWindowView.gameObject.SetActive(true);        
    }

    public void TryClosePhone()
    {
        view.WappWindowView.gameObject.SetActive(false);        
    }
}
