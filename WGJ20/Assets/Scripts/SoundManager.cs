﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource sfxAudioSource;
    [SerializeField] private AudioSource musicAudioSource;
       
    public void PlaySfx(string sound)
    {
        Debug.Log("SoundManager.PlaySfx" + sound);
        sfxAudioSource.clip = Resources.Load<AudioClip>("Audio/Sfx/"+sound);
        sfxAudioSource.loop = false;
        sfxAudioSource.Play();
    }

    public void PlayMusic(string music)
    {
        musicAudioSource.clip = Resources.Load<AudioClip>("Audio/Music/" + music);
        musicAudioSource.loop = true;
        musicAudioSource.Play();
    }
}
